<?php

class GetRss {

	public function constructRss() {

		$rssUrl=($_GET['rssUrl']);

		$rssDoc = new DOMDocument();
		$rssDoc->load($rssUrl);
		$fileName = $_SERVER['DOCUMENT_ROOT']."/txtfiles/RSS_Output_".time().".txt";
		$file = fopen($fileName, "w") or die("Unable to create or find file");
		
		if(!is_null($rssDoc)) {
			$this->retrieveChannel($rssDoc, $file);
			$this->retrieveItems($rssDoc, $file);
		}

		fclose($file);
		exit;

	}

	private function retrieveChannel($rssDoc = null, $file = null) {

		try {
			$channel=$rssDoc->getElementsByTagName('channel')->item(0);

			$channel_title = $this->retrieveRssValue($channel, 'title');
			$channel_link = $this->retrieveRssValue($channel, 'link');
			$channel_desc = $this->retrieveRssValue($channel, 'description');

			$echoChannel = "<p channel><a href='" . $channel_link . "'>" . $channel_title . "</a> <br>" . $channel_desc . "</p>";

			fwrite($file, $echoChannel . "\n");

			echo($echoChannel);

		} catch (Exception $e) {
			echo $e->getMessage();
			exit;
		}
	}

	private function retrieveItems($rssDoc = null, $file = null) {
		try {
			$items = $rssDoc->getElementsByTagName('item');
			$itemCount = $items->length;

			for ($i=0; $i < $itemCount; $i++) {
				$item_title=$this->retrieveRssValue($items->item($i), 'title');
				$item_link=$this->retrieveRssValue($items->item($i), 'link');
				$item_desc=$this->retrieveRssValue($items->item($i), 'description');

				$echoItem = "<php><a href='" . $item_link . "'>" . $item_title . "</a> <br>" . $item_desc . "</p>";
				fwrite($file, $echoItem . "\n");
				echo $echoItem;
			}

		} catch(Exception $e) {
			echo $e->getMessage();
			exit;
		}
	}

	private function retrieveRssValue($object = null, $tag = "") {
		return str_replace("NewsPicks", " ", $object->getElementsByTagName($tag)->item(0)->childNodes->item(0)->nodeValue);
	}
}
?>