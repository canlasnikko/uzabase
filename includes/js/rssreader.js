function loadRss() {
	// initializes XMLHttpRequest
	xmlhttprequest=new XMLHttpRequest();
	xmlhttprequest.onreadystatechange=function() {
		if (this.readyState==4 && this.status==200) {
			document.getElementById("rssOutput").innerHTML=this.responseText;
		}
	}
	xmlhttprequest.open("GET","/includes/php/RetrieveRssHelper.php",true);
	xmlhttprequest.send();
}

$(document).ready(function() {

	$.ajax({
	    data: {
	      rssUrl:"http://tech.uzabase.com/rss"
	    },
	    url: "/includes/php/RetrieveRssHelper.php",
	    dataType: "html",
	    async: false,
	    success: function(data) {
	    	$('#rssOutput').html(data);
	    }
	});

});

